﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Models.ViewModels
{
    public class MemberAllocationPeriodViewModel
    {
        public int MemberId { get; set; }
        public int AllocationPeriodId { get; set; }

        public decimal LeaveDays { get; set; }

        public int CalculateHours()
        {
                var hours = (AllocationPeriod.WorkingDays * 8) -(LeaveDays * 8);
                return Convert.ToInt32(hours);
        }

        public virtual Member Member { get; set; }
        public virtual AllocationPeriod AllocationPeriod { get; set; }
    }
}
