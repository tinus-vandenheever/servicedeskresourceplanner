﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Models.ViewModels
{
    public class ProjectAllocationPeriodViewModel
    {
        public int AllocationPeriodId { get; set; }
        public List<Project> Projects { get; set; }
        public int AvailableHours { get; set; }

        public List<WorkOrder> WorkOrdersInPeriod { get; set; }
        public List<ProjectAllocationPeriod> ProjectAllocationPeriods { get; set; }

        public List<MemberAllocationPeriod> AvailableMembersinPeriod { get; set; }


        public virtual AllocationPeriod AllocationPeriod { get; set; }

        public ProjectAllocationPeriodViewModel(AllocationPeriod allocationPeriod)
        {
            AllocationPeriodId = allocationPeriod.AllocationPeriodId;
        }

        public ProjectAllocationPeriodViewModel()
        {
                
        }
    }

}
