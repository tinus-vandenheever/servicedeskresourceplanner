﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsResourcePlanner.Models.DomainModels
{
    public class AllocationPeriod
    {
        public int AllocationPeriodId { get; set; }
        [MaxLength(150)]
        public string PeriodName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int WorkingDays { get; set; }
    }

    public class AllocationPeriodSelectModel
    {
        public int AllocationPeriodId { get; set; }
        public string PeriodName { get; set; }

        public AllocationPeriodSelectModel(AllocationPeriod allocationPeriod)
        {
            AllocationPeriodId = allocationPeriod.AllocationPeriodId;
            PeriodName = allocationPeriod.PeriodName;
        }
    }
}
