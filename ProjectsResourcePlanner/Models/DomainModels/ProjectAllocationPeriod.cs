﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsResourcePlanner.Models.DomainModels
{
    public class ProjectAllocationPeriod
    {
        [Key, Column(Order = 0)]
        public int ProjectId { get; set; }
        [Key, Column(Order = 1)]
        public int AllocationPeriodId { get; set; }

        public int Hours { get; set; }

        [ForeignKey("MemberId")]
        [Required]
        public int PrimaryAllocatedMemberId { get; set; }

        [ForeignKey("MemberId")]
        [Required]
        public int SecondaryAllocatedMemberId { get; set; }

        [ForeignKey("MemberId")]
        public int? TertiaryAllocatedMemberId { get; set; }

        public decimal PrimaryAllocationPercentage { get; set; }
        public decimal SecondaryAllocationPercentage { get; set; }
        public decimal TertiaryAllocationPercentage { get; set; }

        public virtual Project Project { get; set; }
        public virtual AllocationPeriod AllocationPeriod { get; set; }

        public virtual Member PrimaryAllocatedMember { get; set; }
        public virtual Member SecondaryAllocatedMember { get; set; }
        public virtual Member TertiaryAllocatedMember { get; set; }
    }
}
