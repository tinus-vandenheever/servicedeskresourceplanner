﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsResourcePlanner.Models.DomainModels
{
    public class Project
    {
        public int ProjectId { get; set; }
        [MaxLength(150)]
        public string Name { get; set; }
        public int SLAHours { get; set; }

        public int ChampionMemberId { get; set; }

        [ForeignKey("ChampionMemberId")]
        public virtual Member ChampionMember { get; set; }
    }
}
