﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsResourcePlanner.Models.DomainModels
{
    public class WorkOrder
    {
        public int WorkOrderId { get; set; }

        public int ProjectId { get; set; }

        [Display(Name = "Name/Description")]
        [MaxLength(200)]
        public string Description { get; set; }

        public int TotalHours { get; set; }
        public int MonthlyHours { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project AssociatedProject { get; set; }

    }
}
