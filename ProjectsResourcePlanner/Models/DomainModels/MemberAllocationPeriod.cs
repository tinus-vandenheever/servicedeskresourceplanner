﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ProjectsResourcePlanner.Models.ViewModels;

namespace ProjectsResourcePlanner.Models.DomainModels
{
    public class MemberAllocationPeriod
    {
        [Key, Column(Order = 0)]
        public int MemberId { get; set; }
        [Key, Column(Order = 1)]
        public int AllocationPeriodId { get; set; }

        public int Hours { get; set; }

        public virtual Member Member { get; set; }
        public virtual AllocationPeriod AllocationPeriod { get; set; }

        public MemberAllocationPeriod()
        {
        }
        public MemberAllocationPeriod(MemberAllocationPeriodViewModel memberAllocationPeriodViewModel)
        {
            MemberId = memberAllocationPeriodViewModel.MemberId;
            AllocationPeriodId = memberAllocationPeriodViewModel.AllocationPeriodId;
            Hours = memberAllocationPeriodViewModel.CalculateHours();
        }
    }
}
