﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Models
{
    public class Member
    {
        [Key]
        public int MemberId { get; set; }
        [MaxLength(150)]
        public string Name { get; set; }
        public int AdminHours { get; set; }
        public int TrainingHours { get; set; }

       public virtual ICollection<Project> ChampionProjects { get; set; } 
    }

    public class MemberSelectModel
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }

        public MemberSelectModel(Member member)
        {
            MemberId = member.MemberId;
            MemberName = member.Name;
        }
    }
}
