﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectsResourcePlanner.Data;
using ProjectsResourcePlanner.Models.DomainModels;
using ProjectsResourcePlanner.Models.ViewModels;

namespace ProjectsResourcePlanner.Controllers
{
    public class MemberAllocationPeriodsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MemberAllocationPeriodsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: MemberAllocationPeriods
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.MemberAllocationPeriods.Include(m => m.AllocationPeriod).Include(m => m.Member);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: MemberAllocationPeriods/Details/5
        public async Task<IActionResult> Details(int memberId, int allocationPeriodId)
        {
           
            var memberAllocationPeriod = await _context.MemberAllocationPeriods
                .Include(m => m.AllocationPeriod)
                .Include(m => m.Member)
                .FirstOrDefaultAsync(m => m.MemberId == memberId && m.AllocationPeriodId == allocationPeriodId);
            if (memberAllocationPeriod == null)
            {
                return NotFound();
            }

            return View(memberAllocationPeriod);
        }

        // GET: MemberAllocationPeriods/Create
        public IActionResult Create()
        {
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId");
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
            return View();
        }

        // POST: MemberAllocationPeriods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MemberAllocationPeriodViewModel memberAllocationPeriodViewModel)
        {
            if (ModelState.IsValid)
            {
                memberAllocationPeriodViewModel.AllocationPeriod =
                    _context.AllocationPeriods.Find(memberAllocationPeriodViewModel.AllocationPeriodId);
                var memberAllocationPeriod = new MemberAllocationPeriod(memberAllocationPeriodViewModel);
                _context.Add(memberAllocationPeriod);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", memberAllocationPeriodViewModel.AllocationPeriodId);
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", memberAllocationPeriodViewModel.MemberId);
            return View(memberAllocationPeriodViewModel);
        }

        // GET: MemberAllocationPeriods/Edit/5
        public async Task<IActionResult> Edit(int memberId, int allocationPeriodId)
        {

            var memberAllocationPeriod = await _context.MemberAllocationPeriods.FindAsync(memberId, allocationPeriodId );
            if (memberAllocationPeriod == null)
            {
                return NotFound();
            }
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", memberAllocationPeriod.AllocationPeriodId);
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", memberAllocationPeriod.MemberId);
            return View(memberAllocationPeriod);
        }

        // POST: MemberAllocationPeriods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MemberId,AllocationPeriodId,Hours")] MemberAllocationPeriod memberAllocationPeriod)
        {
            if (id != memberAllocationPeriod.MemberId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(memberAllocationPeriod);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MemberAllocationPeriodExists(memberAllocationPeriod.MemberId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", memberAllocationPeriod.AllocationPeriodId);
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", memberAllocationPeriod.MemberId);
            return View(memberAllocationPeriod);
        }

        // GET: MemberAllocationPeriods/Delete/5
        public async Task<IActionResult> Delete(int memberId, int allocationPeriodId)
        {

            var memberAllocationPeriod = await _context.MemberAllocationPeriods
                .Include(m => m.AllocationPeriod)
                .Include(m => m.Member)
                .FirstOrDefaultAsync(m => m.MemberId == memberId && m.AllocationPeriodId == allocationPeriodId);
            if (memberAllocationPeriod == null)
            {
                return NotFound();
            }

            return View(memberAllocationPeriod);
        }

        // POST: MemberAllocationPeriods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int memberId, int allocationPeriodId)
        {
            var memberAllocationPeriod = await _context.MemberAllocationPeriods.FindAsync(memberId, allocationPeriodId);
            _context.MemberAllocationPeriods.Remove(memberAllocationPeriod);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MemberAllocationPeriodExists(int id)
        {
            return _context.MemberAllocationPeriods.Any(e => e.MemberId == id);
        }
    }
}
