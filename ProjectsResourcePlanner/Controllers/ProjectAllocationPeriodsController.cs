﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectsResourcePlanner.Data;
using ProjectsResourcePlanner.Models;
using ProjectsResourcePlanner.Models.DomainModels;
using ProjectsResourcePlanner.Models.ViewModels;

namespace ProjectsResourcePlanner.Controllers
{
    public class ProjectAllocationPeriodsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProjectAllocationPeriodsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ProjectAllocationPeriods
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProjectAllocationPeriods.Include(p => p.AllocationPeriod).Include(p => p.PrimaryAllocatedMember).Include(p => p.Project).Include(p => p.SecondaryAllocatedMember).Include(p => p.TertiaryAllocatedMember);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ProjectAllocationPeriods/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectAllocationPeriod = await _context.ProjectAllocationPeriods
                .Include(p => p.AllocationPeriod)
                .Include(p => p.PrimaryAllocatedMember)
                .Include(p => p.Project)
                .Include(p => p.SecondaryAllocatedMember)
                .Include(p => p.TertiaryAllocatedMember)
                .FirstOrDefaultAsync(m => m.ProjectId == id);
            if (projectAllocationPeriod == null)
            {
                return NotFound();
            }

            return View(projectAllocationPeriod);
        }

        // GET: ProjectAllocationPeriods/Create
        public IActionResult Create(int allocationPeriodId)
        {
            var allocationPeriod = _context.AllocationPeriods.Find(allocationPeriodId);

            var model = new ProjectAllocationPeriodViewModel(allocationPeriod);

            model.WorkOrdersInPeriod = _context.WorkOrders.Where(x => x.DueDate >= allocationPeriod.StartDate).ToList();
            model.AvailableMembersinPeriod = _context.MemberAllocationPeriods.Where(x => x.AllocationPeriodId == allocationPeriodId).ToList();
            model.Projects = _context.Projects.ToList();
            // model.AvailableHours = GetHoursForAllocationPeriod(allocationPeriod);
            model.ProjectAllocationPeriods = new List<ProjectAllocationPeriod>();

            foreach (var project in model.Projects)
            {
                var pap = new ProjectAllocationPeriod()
                {
                    AllocationPeriodId = allocationPeriod.AllocationPeriodId,
                    AllocationPeriod = allocationPeriod,
                    Hours = project.SLAHours,
                    ProjectId = project.ProjectId,
                    Project = project
                };
                model.ProjectAllocationPeriods.Add(pap);
            }
            ModelState.AddModelError("Member not specified", "Please ensure that a primary and secondary member has been specified for each project.");



            //ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId");
            //ViewData["PrimaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
            //ViewData["ProjectId"] = new SelectList(_context.Projects, "ProjectId", "ProjectId");
            //ViewData["SecondaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
            //ViewData["TertiaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
            return View(model);
        }

        private int GetHoursForAllocationPeriod(AllocationPeriod allocationPeriod, ICollection<Member> members)
        {
            var totalHours = (allocationPeriod.WorkingDays * 8 * members.Count); // -Leave day hours
            return totalHours;
        }

        // POST: ProjectAllocationPeriods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ICollection<ProjectAllocationPeriod> projectAllocationPeriodVm)
        {
            if (projectAllocationPeriodVm.Any(x => x.PrimaryAllocatedMemberId == 0 || x.SecondaryAllocatedMemberId == 0))
            {
                ModelState.AddModelError("Member not specified", "Please ensure that a primary and secondary member has been specified for each project.");
                var allocationPeriodId = projectAllocationPeriodVm.First().AllocationPeriodId;
                var allocationPeriod = _context.AllocationPeriods.Find(allocationPeriodId);
                var model = new ProjectAllocationPeriodViewModel(allocationPeriod);

                model.WorkOrdersInPeriod = _context.WorkOrders.Where(x => x.DueDate >= allocationPeriod.StartDate).ToList();
                model.AvailableMembersinPeriod = _context.MemberAllocationPeriods.Where(x => x.AllocationPeriodId == allocationPeriodId).ToList();
                model.Projects = _context.Projects.ToList();
                model.ProjectAllocationPeriods = new List<ProjectAllocationPeriod>();
                model.ProjectAllocationPeriods.AddRange(projectAllocationPeriodVm);

                return View(model);
            }
            if (ModelState.IsValid)
            {
                var projectAllocationPeriods = new List<ProjectAllocationPeriod>();
                foreach (var pap in projectAllocationPeriodVm)
                {
                    if (pap.PrimaryAllocatedMemberId == 0 || pap.SecondaryAllocatedMemberId == 0)
                    {
                        throw new Exception("Please specify a member in every column. No primary or secondary member specified for ProjectId: " + pap.ProjectId);
                    }
                    var papToAdd = new ProjectAllocationPeriod()
                    {
                        AllocationPeriodId = pap.AllocationPeriodId,
                        ProjectId = pap.ProjectId,
                        Hours = pap.Hours,
                        PrimaryAllocatedMemberId = pap.PrimaryAllocatedMemberId,
                        PrimaryAllocationPercentage = pap.PrimaryAllocationPercentage,
                        SecondaryAllocatedMemberId = pap.SecondaryAllocatedMemberId,
                        SecondaryAllocationPercentage = pap.SecondaryAllocationPercentage,
                        TertiaryAllocatedMemberId = pap.TertiaryAllocatedMemberId,
                        TertiaryAllocationPercentage = pap.TertiaryAllocationPercentage
                    };
                    projectAllocationPeriods.Add(papToAdd);
                }
                _context.Add(projectAllocationPeriodVm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", projectAllocationPeriodVm.AllocationPeriodId);
            //ViewData["PrimaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriodVm.PrimaryAllocatedMemberId);
            //ViewData["ProjectId"] = new SelectList(_context.Projects, "ProjectId", "ProjectId", projectAllocationPeriodVm.ProjectId);
            //ViewData["SecondaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriodVm.SecondaryAllocatedMemberId);
            //ViewData["TertiaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriodVm.TertiaryAllocatedMemberId);
            return View(projectAllocationPeriodVm);
        }

        // GET: ProjectAllocationPeriods/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectAllocationPeriod = await _context.ProjectAllocationPeriods.FindAsync(id);
            if (projectAllocationPeriod == null)
            {
                return NotFound();
            }
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", projectAllocationPeriod.AllocationPeriodId);
            ViewData["PrimaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.PrimaryAllocatedMemberId);
            ViewData["ProjectId"] = new SelectList(_context.Projects, "ProjectId", "ProjectId", projectAllocationPeriod.ProjectId);
            ViewData["SecondaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.SecondaryAllocatedMemberId);
            ViewData["TertiaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.TertiaryAllocatedMemberId);
            return View(projectAllocationPeriod);
        }

        // POST: ProjectAllocationPeriods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProjectId,AllocationPeriodId,Hours,PrimaryAllocatedMemberId,SecondaryAllocatedMemberId,TertiaryAllocatedMemberId,PrimaryAllocationPercentage,SecondaryAllocationPercentage,TertiaryAllocationPercentage")] ProjectAllocationPeriod projectAllocationPeriod)
        {
            if (id != projectAllocationPeriod.ProjectId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(projectAllocationPeriod);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectAllocationPeriodExists(projectAllocationPeriod.ProjectId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AllocationPeriodId"] = new SelectList(_context.AllocationPeriods, "AllocationPeriodId", "AllocationPeriodId", projectAllocationPeriod.AllocationPeriodId);
            ViewData["PrimaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.PrimaryAllocatedMemberId);
            ViewData["ProjectId"] = new SelectList(_context.Projects, "ProjectId", "ProjectId", projectAllocationPeriod.ProjectId);
            ViewData["SecondaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.SecondaryAllocatedMemberId);
            ViewData["TertiaryAllocatedMemberId"] = new SelectList(_context.Members, "MemberId", "MemberId", projectAllocationPeriod.TertiaryAllocatedMemberId);
            return View(projectAllocationPeriod);
        }

        // GET: ProjectAllocationPeriods/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projectAllocationPeriod = await _context.ProjectAllocationPeriods
                .Include(p => p.AllocationPeriod)
                .Include(p => p.PrimaryAllocatedMember)
                .Include(p => p.Project)
                .Include(p => p.SecondaryAllocatedMember)
                .Include(p => p.TertiaryAllocatedMember)
                .FirstOrDefaultAsync(m => m.ProjectId == id);
            if (projectAllocationPeriod == null)
            {
                return NotFound();
            }

            return View(projectAllocationPeriod);
        }

        // POST: ProjectAllocationPeriods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var projectAllocationPeriod = await _context.ProjectAllocationPeriods.FindAsync(id);
            _context.ProjectAllocationPeriods.Remove(projectAllocationPeriod);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjectAllocationPeriodExists(int id)
        {
            return _context.ProjectAllocationPeriods.Any(e => e.ProjectId == id);
        }
    }
}
