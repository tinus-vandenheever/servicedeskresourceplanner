﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectsResourcePlanner.Data;
using ProjectsResourcePlanner.Models;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Controllers
{
    public class AllocationPeriodsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AllocationPeriodsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AllocationPeriods
        public async Task<IActionResult> Index()
        {
            return View(await _context.AllocationPeriods.ToListAsync());
        }

        // GET: AllocationPeriods/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allocationPeriod = await _context.AllocationPeriods
                .FirstOrDefaultAsync(m => m.AllocationPeriodId == id);
            if (allocationPeriod == null)
            {
                return NotFound();
            }

            return View(allocationPeriod);
        }

        // GET: AllocationPeriods/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AllocationPeriods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AllocationPeriodId,PeriodName,StartDate,EndDate,WorkingDays")] AllocationPeriod allocationPeriod)
        {
            if (ModelState.IsValid)
            {
                _context.Add(allocationPeriod);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(allocationPeriod);
        }

        // GET: AllocationPeriods/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allocationPeriod = await _context.AllocationPeriods.FindAsync(id);
            if (allocationPeriod == null)
            {
                return NotFound();
            }
            return View(allocationPeriod);
        }

        // POST: AllocationPeriods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AllocationPeriodId,PeriodName,StartDate,EndDate,WorkingDays")] AllocationPeriod allocationPeriod)
        {
            if (id != allocationPeriod.AllocationPeriodId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(allocationPeriod);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AllocationPeriodExists(allocationPeriod.AllocationPeriodId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(allocationPeriod);
        }

        // GET: AllocationPeriods/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var allocationPeriod = await _context.AllocationPeriods
                .FirstOrDefaultAsync(m => m.AllocationPeriodId == id);
            if (allocationPeriod == null)
            {
                return NotFound();
            }

            return View(allocationPeriod);
        }

        // POST: AllocationPeriods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var allocationPeriod = await _context.AllocationPeriods.FindAsync(id);
            _context.AllocationPeriods.Remove(allocationPeriod);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AllocationPeriodExists(int id)
        {
            return _context.AllocationPeriods.Any(e => e.AllocationPeriodId == id);
        }

        public async Task<IActionResult> GetAllPeriods(string searchTerm)
        {
            var allocationPeriods = new List<AllocationPeriod>();
            if (!(string.IsNullOrEmpty(searchTerm) || string.IsNullOrWhiteSpace(searchTerm)))
            {
                allocationPeriods = await _context.AllocationPeriods
                    .Where(x => x.PeriodName.ToLowerInvariant().Contains(searchTerm.ToLowerInvariant())).ToListAsync();
            }
            
            var allocationPeriodSelectList = new List<AllocationPeriodSelectModel>();
            foreach (var allocationPeriod in allocationPeriods)
            {
                allocationPeriodSelectList.Add(new AllocationPeriodSelectModel(allocationPeriod));
            }

            return Json(new { items = allocationPeriodSelectList });
        }
    }
}
