﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectsResourcePlanner.Data.Migrations
{
    public partial class AddedAllocationPeriodModelsAGAIN2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllocationPeriods",
                columns: table => new
                {
                    AllocationPeriodId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PeriodName = table.Column<string>(maxLength: 150, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    WorkingDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllocationPeriods", x => x.AllocationPeriodId);
                });

            migrationBuilder.CreateTable(
                name: "ProjectAllocationPeriods",
                columns: table => new
                {
                    ProjectId = table.Column<int>(nullable: false),
                    AllocationPeriodId = table.Column<int>(nullable: false),
                    Hours = table.Column<int>(nullable: false),
                    PrimaryAllocatedMemberId = table.Column<int>(nullable: false),
                    SecondaryAllocatedMemberId = table.Column<int>(nullable: false),
                    TertiaryAllocatedMemberId = table.Column<int>(nullable: true),
                    PrimaryAllocationPercentage = table.Column<decimal>(type: "decimal(19,4)", nullable: false),
                    SecondaryAllocationPercentage = table.Column<decimal>(type: "decimal(19,4)", nullable: false),
                    TertiaryAllocationPercentage = table.Column<decimal>(type: "decimal(19,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectAllocationPeriods", x => new { x.ProjectId, x.AllocationPeriodId });
                    table.UniqueConstraint("AK_ProjectAllocationPeriods_AllocationPeriodId_ProjectId", x => new { x.AllocationPeriodId, x.ProjectId });
                    table.ForeignKey(
                        name: "FK_ProjectAllocationPeriods_AllocationPeriods_AllocationPeriodId",
                        column: x => x.AllocationPeriodId,
                        principalTable: "AllocationPeriods",
                        principalColumn: "AllocationPeriodId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectAllocationPeriods_Members_PrimaryAllocatedMemberId",
                        column: x => x.PrimaryAllocatedMemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectAllocationPeriods_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "ProjectId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectAllocationPeriods_Members_SecondaryAllocatedMemberId",
                        column: x => x.SecondaryAllocatedMemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectAllocationPeriods_Members_TertiaryAllocatedMemberId",
                        column: x => x.TertiaryAllocatedMemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectAllocationPeriods_PrimaryAllocatedMemberId",
                table: "ProjectAllocationPeriods",
                column: "PrimaryAllocatedMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectAllocationPeriods_SecondaryAllocatedMemberId",
                table: "ProjectAllocationPeriods",
                column: "SecondaryAllocatedMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectAllocationPeriods_TertiaryAllocatedMemberId",
                table: "ProjectAllocationPeriods",
                column: "TertiaryAllocatedMemberId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectAllocationPeriods");

            migrationBuilder.DropTable(
                name: "AllocationPeriods");
        }
    }
}
