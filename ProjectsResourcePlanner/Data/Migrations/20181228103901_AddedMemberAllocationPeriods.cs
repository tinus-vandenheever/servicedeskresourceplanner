﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectsResourcePlanner.Data.Migrations
{
    public partial class AddedMemberAllocationPeriods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MemberAllocationPeriods",
                columns: table => new
                {
                    MemberId = table.Column<int>(nullable: false),
                    AllocationPeriodId = table.Column<int>(nullable: false),
                    Hours = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberAllocationPeriods", x => new { x.MemberId, x.AllocationPeriodId });
                    table.UniqueConstraint("AK_MemberAllocationPeriods_AllocationPeriodId_MemberId", x => new { x.AllocationPeriodId, x.MemberId });
                    table.ForeignKey(
                        name: "FK_MemberAllocationPeriods_AllocationPeriods_AllocationPeriodId",
                        column: x => x.AllocationPeriodId,
                        principalTable: "AllocationPeriods",
                        principalColumn: "AllocationPeriodId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberAllocationPeriods_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Restrict);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MemberAllocationPeriods");
        }
    }
}
