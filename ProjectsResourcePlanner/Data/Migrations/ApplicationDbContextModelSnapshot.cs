﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProjectsResourcePlanner.Data;

namespace ProjectsResourcePlanner.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderKey")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider")
                        .HasMaxLength(128);

                    b.Property<string>("Name")
                        .HasMaxLength(128);

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.AllocationPeriod", b =>
                {
                    b.Property<int>("AllocationPeriodId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EndDate");

                    b.Property<string>("PeriodName")
                        .HasMaxLength(150);

                    b.Property<DateTime>("StartDate");

                    b.Property<int>("WorkingDays");

                    b.HasKey("AllocationPeriodId");

                    b.ToTable("AllocationPeriods");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.MemberAllocationPeriod", b =>
                {
                    b.Property<int>("MemberId");

                    b.Property<int>("AllocationPeriodId");

                    b.Property<int>("Hours");

                    b.HasKey("MemberId", "AllocationPeriodId");

                    b.HasAlternateKey("AllocationPeriodId", "MemberId");

                    b.ToTable("MemberAllocationPeriods");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.Project", b =>
                {
                    b.Property<int>("ProjectId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ChampionMemberId");

                    b.Property<string>("Name")
                        .HasMaxLength(150);

                    b.Property<int>("SLAHours");

                    b.HasKey("ProjectId");

                    b.HasIndex("ChampionMemberId");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.ProjectAllocationPeriod", b =>
                {
                    b.Property<int>("ProjectId");

                    b.Property<int>("AllocationPeriodId");

                    b.Property<int>("Hours");

                    b.Property<int>("PrimaryAllocatedMemberId");

                    b.Property<decimal>("PrimaryAllocationPercentage")
                        .HasColumnType("decimal(19,4)");

                    b.Property<int>("SecondaryAllocatedMemberId");

                    b.Property<decimal>("SecondaryAllocationPercentage")
                        .HasColumnType("decimal(19,4)");

                    b.Property<int?>("TertiaryAllocatedMemberId");

                    b.Property<decimal>("TertiaryAllocationPercentage")
                        .HasColumnType("decimal(19,4)");

                    b.HasKey("ProjectId", "AllocationPeriodId");

                    b.HasAlternateKey("AllocationPeriodId", "ProjectId");

                    b.HasIndex("PrimaryAllocatedMemberId");

                    b.HasIndex("SecondaryAllocatedMemberId");

                    b.HasIndex("TertiaryAllocatedMemberId");

                    b.ToTable("ProjectAllocationPeriods");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.WorkOrder", b =>
                {
                    b.Property<int>("WorkOrderId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<DateTime>("DueDate");

                    b.Property<int>("MonthlyHours");

                    b.Property<int>("ProjectId");

                    b.Property<DateTime>("StartDate");

                    b.Property<int>("TotalHours");

                    b.HasKey("WorkOrderId");

                    b.HasIndex("ProjectId");

                    b.ToTable("WorkOrders");
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.Member", b =>
                {
                    b.Property<int>("MemberId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AdminHours");

                    b.Property<string>("Name")
                        .HasMaxLength(150);

                    b.Property<int>("TrainingHours");

                    b.HasKey("MemberId");

                    b.ToTable("Members");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.MemberAllocationPeriod", b =>
                {
                    b.HasOne("ProjectsResourcePlanner.Models.DomainModels.AllocationPeriod", "AllocationPeriod")
                        .WithMany()
                        .HasForeignKey("AllocationPeriodId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ProjectsResourcePlanner.Models.Member", "Member")
                        .WithMany()
                        .HasForeignKey("MemberId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.Project", b =>
                {
                    b.HasOne("ProjectsResourcePlanner.Models.Member", "ChampionMember")
                        .WithMany("ChampionProjects")
                        .HasForeignKey("ChampionMemberId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.ProjectAllocationPeriod", b =>
                {
                    b.HasOne("ProjectsResourcePlanner.Models.DomainModels.AllocationPeriod", "AllocationPeriod")
                        .WithMany()
                        .HasForeignKey("AllocationPeriodId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ProjectsResourcePlanner.Models.Member", "PrimaryAllocatedMember")
                        .WithMany()
                        .HasForeignKey("PrimaryAllocatedMemberId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ProjectsResourcePlanner.Models.DomainModels.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ProjectsResourcePlanner.Models.Member", "SecondaryAllocatedMember")
                        .WithMany()
                        .HasForeignKey("SecondaryAllocatedMemberId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ProjectsResourcePlanner.Models.Member", "TertiaryAllocatedMember")
                        .WithMany()
                        .HasForeignKey("TertiaryAllocatedMemberId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ProjectsResourcePlanner.Models.DomainModels.WorkOrder", b =>
                {
                    b.HasOne("ProjectsResourcePlanner.Models.DomainModels.Project", "AssociatedProject")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
