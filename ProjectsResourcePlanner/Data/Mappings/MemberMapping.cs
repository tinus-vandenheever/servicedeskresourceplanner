﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectsResourcePlanner.Models;

namespace ProjectsResourcePlanner.Data.Mappings
{
    public class MemberMapping : IEntityTypeConfiguration<Member>
    {
        public void Configure(EntityTypeBuilder<Member> builder)
        {
            builder.HasKey(c => c.MemberId);
            builder.HasMany(x => x.ChampionProjects).WithOne(x => x.ChampionMember);

        }
    }
}
