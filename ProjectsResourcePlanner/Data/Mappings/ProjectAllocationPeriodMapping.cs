﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Data.Mappings
{
    public class ProjectAllocationPeriodMapping : IEntityTypeConfiguration<ProjectAllocationPeriod>
    {
        public void Configure(EntityTypeBuilder<ProjectAllocationPeriod> builder)
        {
            builder.HasKey(c => new { c.ProjectId, c.AllocationPeriodId });

            builder.HasOne(x => x.Project).WithMany().HasForeignKey(x => x.ProjectId);
            builder.HasOne(x => x.AllocationPeriod).WithMany().HasForeignKey(x => x.AllocationPeriodId);

            builder.Property(x => x.PrimaryAllocationPercentage).HasColumnType("decimal(19,4)");
            builder.Property(x => x.SecondaryAllocationPercentage).HasColumnType("decimal(19,4)");
            builder.Property(x => x.TertiaryAllocationPercentage).HasColumnType("decimal(19,4)");
            
        }
    }
}
