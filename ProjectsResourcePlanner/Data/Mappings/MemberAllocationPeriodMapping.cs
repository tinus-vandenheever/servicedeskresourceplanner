﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Data.Mappings
{
    public class MemberAllocationPeriodMapping : IEntityTypeConfiguration<MemberAllocationPeriod>
    {
        public void Configure(EntityTypeBuilder<MemberAllocationPeriod> memberAllocationPeriod)
        {
            memberAllocationPeriod.HasKey(c => new { c.MemberId, c.AllocationPeriodId });

            memberAllocationPeriod.HasOne(x => x.Member).WithMany().HasForeignKey(x => x.MemberId);
            memberAllocationPeriod.HasOne(x => x.AllocationPeriod).WithMany().HasForeignKey(x => x.AllocationPeriodId);
            
        }
    }
}
