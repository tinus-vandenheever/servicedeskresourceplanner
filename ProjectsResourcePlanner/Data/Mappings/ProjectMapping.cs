﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Data.Mappings
{
    public class ProjectMapping : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(c => c.ProjectId);
            builder.HasOne(x => x.ChampionMember).WithMany(x => x.ChampionProjects);

        }
    }
}
