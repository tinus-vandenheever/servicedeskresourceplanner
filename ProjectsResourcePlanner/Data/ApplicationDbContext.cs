﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjectsResourcePlanner.Data.Mappings;
using ProjectsResourcePlanner.Models;
using ProjectsResourcePlanner.Models.DomainModels;

namespace ProjectsResourcePlanner.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.ApplyConfiguration(new ProjectAllocationPeriodMapping());
            modelBuilder.ApplyConfiguration(new MemberAllocationPeriodMapping());

        }

        public DbSet<Member> Members { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
        public DbSet<AllocationPeriod> AllocationPeriods { get; set; }
        public DbSet<ProjectAllocationPeriod> ProjectAllocationPeriods { get; set; }
        public DbSet<MemberAllocationPeriod> MemberAllocationPeriods { get; set; }
    }
}
